drop materialized view if exists foreign_table_column_by_primary_table;

create materialized view foreign_table_column_by_primary_table as (
                                                                  select  unique_table.table_schema || '.' || unique_table.table_name as primary_table,
                                                                          fk_table.table_schema || '.' ||fk_table.table_name                            as foreign_table,
                                                                          fk_table.column_name                                                as fk_column,
                                                                          fk_table.constraint_name
                                                                  from information_schema.key_column_usage unique_table
                                                                           inner join information_schema.referential_constraints
                                                                                      on unique_table.constraint_schema = referential_constraints.unique_constraint_schema
                                                                                          and unique_table.table_catalog = referential_constraints.unique_constraint_catalog
                                                                                          and unique_table.constraint_name = referential_constraints.unique_constraint_name
                                                                           join information_schema.key_column_usage fk_table
                                                                                on fk_table.constraint_schema = referential_constraints.constraint_schema
                                                                                    and fk_table.table_catalog = referential_constraints.constraint_catalog
                                                                                    and fk_table.constraint_name = referential_constraints.constraint_name
                                                                  where unique_table.table_schema = 'public'
                                                                    and unique_table.constraint_schema = 'public'
                                                                    and unique_table.table_catalog = 'my_catalog'
                                                                    and unique_table.constraint_catalog = 'my_catalog'
                                                                  order by unique_table.table_schema, unique_table.table_name,
                                                                           fk_table.table_schema, fk_table.table_name, fk_table.column_name);

create or replace function replace_fk_for_pk_table( p_table_name varchar, p_old_id bigint, p_new_id bigint)
    returns text as $$
declare
    trace text default '';
    rec_fk_table record;
    update_count int;
    cur_fk_table refcursor;
begin
    -- open the cursor
    open cur_fk_table for execute format (
            'select foreign_table, fk_column from public.foreign_table_column_by_primary_table where primary_table = $1'
                                  ) USING p_table_name;

    loop
        -- fetch row into the rec_fk_table
        fetch cur_fk_table into rec_fk_table;
        -- exit when no more row to fetch
        exit when not found;

        EXECUTE format('update ' || rec_fk_table.foreign_table ||' set '||rec_fk_table.fk_column||' = %s where '||rec_fk_table.fk_column||' = %s', p_new_id, p_old_id);
        get diagnostics update_count=row_count ;
        trace := trace || ',' || rec_fk_table.foreign_table || ':' || rec_fk_table.fk_column || ':' || update_count;
        RAISE INFO 'update %.% % rows',rec_fk_table.foreign_table,rec_fk_table.fk_column, update_count;
--         RAISE INFO 'update %  set % = % where % = %;',rec_fk_table.foreign_table, rec_fk_table.fk_column, p_new_id,rec_fk_table.fk_column, p_old_id;


    end loop;

    -- close the cursor
    close cur_fk_table;

    return trace;
end; $$

    language plpgsql;
