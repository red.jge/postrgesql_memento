# fk_updater

## Problématique

Mettre à jour automatiquement toutes les clés étrangères à partir d'une clé primaire,
en se basant sur les contraintes du modèle.

La fonction nécessite l'existence d'une vue matérialisée `foreign_table_column_by_primary_table` 
qui permet de retrouver instantanément les tables liées par des contraintes de clée étrangére. 
La création de la vue matérialisée peut prendre plusieurs minutes.

## Mise en place

exécuter le [script](fk_updater.sql) afin de créer la vue matérialisé puis la function.

## Exemple d'utilisation 

```sql
select replace_fk_for_pk_table('public.my_table',4,6);

delete from public.my_table where id=4;
```
